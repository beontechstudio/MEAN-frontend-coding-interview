import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'orders',
    loadChildren: () => import('./modules/orders/orders.module')
      .then(m => m.OrdersModule)
  },
  {
    path: 'pizzas',
    loadChildren: () => import('./modules/pizzas/pizzas.module')
      .then(m => m.PizzasModule)
  },
  {
    path: '**',
    redirectTo: '/orders'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
