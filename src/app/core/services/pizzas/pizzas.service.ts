import { Inject, Injectable } from '@angular/core';
import { ENV, environment } from "~env";
import { HttpClient } from "@angular/common/http";
import { PizzasStore } from "~app/core/state/pizzas.store";
import { PizzaModel } from "~app/core/state/pizza.model";
import { map, pluck, tap } from "rxjs/operators";
import { Observable } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class PizzasService {
  pizzas$: Observable<PizzaModel[]> = this.pizzasStore.find();

  constructor(
    @Inject(ENV)
    private readonly env: typeof environment,
    private readonly http: HttpClient,
    private readonly pizzasStore: PizzasStore,
  ) { }

  getPizzas(): Observable<PizzaModel[]> {
    this.pizzasStore.update(state => ({ ...state, requestingPizzas: true }));
    return this.http.get<{ pizzas: PizzaModel[] }>(`${this.env.api}/pizzas`)
      .pipe(pluck('pizzas'))
      .pipe(map(pizzas => {
        this.pizzasStore.add(pizzas);
        this.pizzasStore.update(state => ({ ...state, requestingPizzas: false }));
        return pizzas;
      }));
  }

  activatePizza(pizzaId: PizzaModel['_id']): Observable<PizzaModel> {
    return this.http.put<{ pizza: PizzaModel }>(`${this.env.api}/pizzas/${pizzaId}/activate`, {})
      .pipe(pluck('pizza'))
      .pipe(map(pizza => {
        this.pizzasStore.upsert(pizza._id, pizza);
        return pizza;
      }))
  }

  disablePizza(pizzaId: PizzaModel['_id']): Observable<PizzaModel> {
    return this.http.put<{ pizza: PizzaModel }>(`${this.env.api}/pizzas/${pizzaId}/disable`, {})
      .pipe(pluck('pizza'))
      .pipe(map(pizza => {
        this.pizzasStore.upsert(pizza._id, pizza);
        return pizza;
      }))
  }
}
