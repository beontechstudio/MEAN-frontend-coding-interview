import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, ReplaySubject } from "rxjs";
import { map, pluck } from "rxjs/operators";

// NOTE: All entities must have an _id value
export class BaseStore<T, R> {
  public stateInitiated$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  private state: BehaviorSubject<T & {
    entities: { [_id: string]: R },
  }>;

  constructor(initialState: T) {
    this.state = new BehaviorSubject({
      ...initialState,
      entities: {},
    });
    this.stateInitiated$.next(true);
  }

  select(field: keyof T) {
    return this.state.asObservable()
      .pipe(pluck(field));
  }

  find(): Observable<R[]> {
    return this.state.asObservable()
      .pipe(pluck('entities'))
      .pipe(map(entities => Object.values(entities)));
  }

  update(newState: (state: T) => T): void {
    const currentState = this.state.getValue();
    const updatedState = newState(currentState);
    this.state.next({
      ...currentState,
      ...updatedState,
    });
  }

  /*
  * This method push new entities to the state's store
  * */
  add(data: R | R[]): void {
    const currentState = this.state.getValue();
    const newEntities = (Array.isArray(data) ? data : [])
      .reduce((all, current) => ({
        ...all,
        [(current as any)._id]: current
      }), {});

    this.state.next({
      ...currentState,
      entities: {
        ...currentState.entities,
        ...newEntities
      },
    });
  }

  upsert(entityId: string, entity: R): void {
    const currentState = this.state.getValue();
    currentState.entities[entityId] = entity;
    this.state.next(currentState);
  }
}

declare type IBaseState<T, R> = T & {
  entities?: { [_id: string]: R },
};
