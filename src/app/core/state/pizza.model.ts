export interface PizzaModel {
  _id: string;

  name: string;

  baseIngredients: Array<{
    ingredientId: string;
    name: string;
  }> ;

  basePrice: number;

  isAvailable: boolean;

  cheeseBorder: boolean;

  createdAt?: Date;
  updatedAt?: Date;
}
