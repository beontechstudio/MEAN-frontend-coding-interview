import { TestBed } from '@angular/core/testing';

import { PizzasStore } from './pizzas.store';

describe('PizzasService', () => {
  let service: PizzasStore;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PizzasStore);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
