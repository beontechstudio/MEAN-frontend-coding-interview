import { Injectable } from '@angular/core';
import { BaseStore } from "./base.store";
import { PizzaModel } from "./pizza.model";

const initialState = {
  requestingPizzas: false,
  updatingPizza: false,
};

@Injectable({
  providedIn: 'root'
})
export class PizzasStore extends BaseStore<typeof initialState, PizzaModel> {
  constructor() {
    super(initialState);
  }
}
