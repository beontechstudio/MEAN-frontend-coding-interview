import { Component, Input, OnInit } from '@angular/core';
import { OrderModel } from "~app/core/state/order.model";
import { ReplaySubject } from "rxjs";

@Component({
  selector: 'app-order-card',
  templateUrl: './order-card.component.html',
  styleUrls: ['./order-card.component.scss']
})
export class OrderCardComponent implements OnInit {
  order$: ReplaySubject<OrderModel> = new ReplaySubject<OrderModel>();
  @Input() set order(data: OrderModel) {
    this.order$.next(data);
  }

  constructor() { }

  ngOnInit(): void {
  }

}
