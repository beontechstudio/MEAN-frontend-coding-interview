import { Component, OnInit } from '@angular/core';
import { OrdersService } from "~app/core/services/orders/orders.service";

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {
  orders$ = this.ordersService.orders$;

  constructor(
    private readonly ordersService: OrdersService,
  ) { }

  ngOnInit(): void {
    this.ordersService.getOrders()
      .subscribe()
  }

}
