import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PizzasRoutingModule } from './pizzas-routing.module';
import { PizzasComponent } from './pages/pizzas/pizzas.component';
import { PizzaComponent } from './pages/pizza/pizza.component';
import { PizzaCardComponent } from './components/pizza-card/pizza-card.component';


@NgModule({
  declarations: [
    PizzasComponent,
    PizzaComponent,
    PizzaCardComponent
  ],
  imports: [
    CommonModule,
    PizzasRoutingModule
  ]
})
export class PizzasModule { }
