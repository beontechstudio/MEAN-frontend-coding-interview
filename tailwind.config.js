module.exports = {
  prefix: '',
  purge: {
    enabled: process.env.NODE_ENV === 'production',
    content: [
      './src/**/*.{html,ts}',
    ]
  },
  darkMode: 'class', // or 'media' or 'class'
  theme: {
    extend: {
      maxHeight: {
        '1/10': '10%',
        '2/10': '20%',
        '3/10': '30%',
        '4/10': '40%',
        '5/10': '50%',
        '6/10': '60%',
        '7/10': '70%',
        '8/10': '80%',
        '9/10': '90%',
      }
    },
    colors: {
      'transparent': 'rgba(0, 0, 0, 0)',
      'dark-primary': '#512DA8',
      'light-primary': '#D1C4E9',
      'primary': '#673AB7',
      'white': '#FFFFFF',
      'accent': '#FF4081',
      'primary-text': '#212121',
      'secondary-text': '#757575',
      'third-text': '#BDBDBD',
    }
  },
  variants: {
    extend: {},
  },
  plugins: [
    // We use the strategy class to avoid undesired classes on elements
    require("@tailwindcss/forms")
  ],
};
